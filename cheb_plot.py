# -*- coding: utf-8 -*-
"""
Created on Thu Aug 15 15:39:13 2019

@author: SurfacePro3
"""

from cheb import cheb
from numpy import *
import matplotlib.pyplot as plt

def chebplot(f,fdash,N):
    mean_error = []
    xx = linspace(1,-1,201)
    uu = f(xx)
    for i in range(1,N):
        D,x = cheb(i)
        u = f(x)
        Du = matmul(D,u)
        Duu = fdash(xx)
        error = Du - fdash(x)
        mean_error.append(mean(error))
    
    #    fig = plt.figure(figsize = (8,6))
    #    ax = fig.add_subplot(1,1,1)
    #    ax.plot(x,u,'o')
    #    ax.plot(xx,uu,linewidth = 2)
        
    #    fig2 = plt.figure(figsize = (8,6))
    #    ax2 = fig2.add_subplot(1,1,1)
    #    ax2.plot(x,Du,'o')
    #    ax2.plot(xx,Duu,linewidth = 2)
    fig3 = plt.figure(figsize = (8,6))
    ax3 = fig3.add_subplot(1,1,1)
    ax3.plot(arange(1,N),mean_error,'o')
    plt.yscale('log')
    plt.grid()
  