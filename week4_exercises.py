# Import everything from numpy
from numpy import *
# Import all the plotting routines
from matplotlib.pyplot import *
# Import the ODE integrator
from scipy.integrate import odeint


def duffing(x, t, pars):
    """
    duffing(x, t, pars)

    Return the right-hand side of the Duffing differential equation in
    first-order form. The equation is given by

        x'' + 2*xi*x' + x + x^3 = Gamma*sin(omega*t)

    Parameters to the function
        x
            State variables [x1, x2]
        t
            Time
        pars
            System parameters [xi, Gamma, omega]
    """
    return [x[1], pars[1]*sin(pars[2]*t) - 2*pars[0]*x[1] - x[0] - x[0]**3]


# Specify the parameters
xi = 0.05
Gamma = 0.2
omega = 1.2
T = 2*pi/omega

# Integrate for a long time
t = linspace(0, 100*T, 5001)  # 5000/100 = 50 points per period (+1 for starting point)
x = odeint(duffing, [0, 0], t, args=([xi, Gamma, omega],))
plot(t, x[:, 0])
xlabel("Time")
ylabel("x")
show()

# Commit to the repository:
#   git add week4-exercises.py
#   git commit -m "Question 1"

# Isolate a single period - this only works because integrating in integer
# multiples of the period ensures that the sin(omega*t) term takes the same
# value at the first point of the new integration run as it did on the last
# point of the old one
x0 = x[-1, :]  # use the last point from the solution as the new starting point
t = linspace(0, T, 101)
x = odeint(duffing, x0, t, args=([xi, Gamma, omega],))
plot(t, x[:, 0])
xlabel("Time")
ylabel("x")
title("Start position: {}, Period: {:.4g}".format(x0, T))
show()

# Commit to the repository:
#   git add week4-exercises.py
#   git commit -m "Question 1"

# Set up a root-finding problem for the Duffing equation: f(x) = 0
def f(x):
    """
    f(x)

    Integrate the Duffing equation over one period starting at x and return the
    difference between the state variables at the end of the period and the
    start of the period.
    """
    return x - odeint(duffing, x, [0, 2*pi/omega], args=([xi, Gamma, omega],))[-1, :]

# Get access to the root-finder
from scipy.optimize import fsolve

# Use the root-finder and show that the solution we found from direct simulation
# is the same as the one we find by shooting
xnew, info, ier, mesg = fsolve(f, x0, full_output=True)
print("**The original start point was: {}".format(x0))
if ier == 1:
    print("The root finder converged")
    print("The corrected start point is: {}".format(xnew))
    print("Which is a difference of: {:.4g}".format(linalg.norm(x0-xnew)))  # Euclidean distance (norm) between the two points
else:
    print("The root finder failed to converge")

# See what happens when we start from a "random" point
x0 = [0.0, 1.0]
xnew, info, ier, mesg = fsolve(f, x0, full_output=True)
print("**The original start point was: {}".format(x0))
if ier == 1:
    print("The root finder converged")
    print("The corrected start point is: {}".format(xnew))
    print("Which is a difference of: {:.4g}".format(linalg.norm(x0-xnew)))  # Euclidean distance (norm) between the two points
else:
    print("The root finder failed to converge")

# Notice that the shooting method does not always converge!
x0 = [0.0, 0.0]
xnew, info, ier, mesg = fsolve(f, x0, full_output=True)
print("**The original start point was: {}".format(x0))
if ier == 1:
    print("The root finder converged")
    print("The corrected start point is: {}".format(xnew))
    print("Which is a difference of: {:.4g}".format(linalg.norm(x0-xnew)))  # Euclidean distance (norm) between the two points
else:
    print("The root finder failed to converge")

# Commit to the repository:
#   git add week4-exercises.py
#   git commit -m "Question 2"

# Use the shooting method to try to find all the possible solutions
xvals = []
for i in range(0, 10):  # 10 random trials
    x0 = 4*random.rand(1, 2) - 2  # random numbers between [-2, 2]
    xnew, info, ier, mesg = fsolve(f, x0, full_output=True)
    if ier == 1:
        xvals.append(xnew)
xvals = sort(xvals)

print(xvals)

# Commit to the repository:
#   git add week4-exercises.py
#   git commit -m "Question 3"

# Generalising (and encapsulating) the code will mean needing to be able to pass
# arbitrary vector fields into the fsolve routine. See week4_exercises_final.
