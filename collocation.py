# -*- coding: utf-8 -*-
"""
Created on Sun Aug 18 15:59:51 2019

@author: SurfacePro3
"""
from cheb import cheb
from scipy.integrate import odeint

def collocation(ode,n,x0,pars):
    D,x = cheb(n)
    soln = odeint(ode,x0,x,(pars,))
    return soln