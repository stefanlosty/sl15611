# -*- coding: utf-8 -*-
"""
Created on Mon Aug 12 18:06:59 2019

@author: SurfacePro3
"""
import numpy as np

def cheb(N):
    if N == 0:
        D = 0
        x = 1
    else:
        j = []
        k = np.zeros(N+1)
        for i in range(N+1):
            k[i] = i
            j.append(np.pi*i/N)
        x = np.cos(j)
        if N > 1:
            b = [[2]]
            bb = np.ones((N-1,1))
            bb = bb.tolist()
            for i in range(N-1):
                b.append([bb[i][0]])      
            b.append([2])
            b = np.array(b)
        else:
            b = np.array([[2],[2]])
        ca = (-1)**np.array(k)
        cb = []
        for i in range(len(ca)):
            cb.append([ca[i]])       
        cc = np.array(cb)
        c = b*cc
        X = np.tile(x,[N+1,1])
        X = np.transpose(X)
        dX = X - np.transpose(X)
        c_inv = []
        for i in range(len(c)):
            c_inv.append(1.0/c[i])
        D1 = np.outer(c,np.transpose(c_inv))
        D2 = (dX+(np.identity(N+1)))
        D = D1/D2
        D -= np.diag(sum(np.transpose(D)))
    return D,x